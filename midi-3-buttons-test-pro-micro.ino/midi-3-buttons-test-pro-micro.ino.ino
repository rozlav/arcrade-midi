#include "MIDIUSB.h"

#define bouton_rouge 2
#define bouton_jaune 3
#define bouton_vert 4
#define led_rouge 5
#define led_vert 7
#define led_jaune 6

bool der_etat_bouton_vert = false;
bool der_etat_bouton_jaune = false;
bool der_etat_bouton_rouge = false;

void controlChange(byte channel, byte control, byte value) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}

void setup() {
  Serial.begin(9600);
  pinMode(bouton_vert, INPUT_PULLUP);
  pinMode(bouton_jaune, INPUT_PULLUP);
  pinMode(bouton_rouge, INPUT_PULLUP);
  pinMode(led_vert, OUTPUT);
  pinMode(led_jaune, OUTPUT);
  pinMode(led_rouge, OUTPUT);
}

void loop() {
  //
  //// BOUTON 4 VERT 💚
  //// LED : 7
  //
  int etat_bouton_vert = digitalRead(bouton_vert);
  if (etat_bouton_vert != der_etat_bouton_vert) {
    if (etat_bouton_vert == true) {
      digitalWrite(led_vert,HIGH);
      Serial.println("vert On");
      controlChange(0, 10, 0);
      MidiUSB.flush();
    } else {
      digitalWrite(led_vert,LOW);
      Serial.println("vert Off");
      controlChange(0, 10, 127);
      MidiUSB.flush();
    }
  }
  der_etat_bouton_vert = etat_bouton_vert;  
  delay(70);        // delay in between reads for stability
  //
  //// BOUTON 3 JAUNE 💛
  //// LED : 6
  //
  int etat_bouton_jaune = digitalRead(bouton_jaune);
  if (etat_bouton_jaune != der_etat_bouton_jaune) {
    if (etat_bouton_jaune == true) {
      digitalWrite(led_jaune,HIGH);
      Serial.println("jaune On");
      controlChange(1, 10, 0);
      MidiUSB.flush();
    } else {
      digitalWrite(led_jaune,LOW);
      Serial.println("Jaune Off");
      controlChange(1, 10, 127);
      MidiUSB.flush();
    }
  }
  der_etat_bouton_jaune = etat_bouton_jaune;  
  delay(70);        // delay in between reads for stability
  //
  //// BOUTON 2 ROUGE ❤️
  //// LED : 5
  //
  int etat_bouton_rouge = digitalRead(bouton_rouge);
  if (etat_bouton_rouge != der_etat_bouton_rouge) {
    if (etat_bouton_rouge == true) {
      digitalWrite(led_rouge,HIGH);
      Serial.println("rouge On");
      controlChange(2, 10, 0);
      MidiUSB.flush();
    } else {
      digitalWrite(led_rouge,LOW);
      Serial.println("rouge Off");
      controlChange(2, 10, 127);
      MidiUSB.flush();
    }
  }
  der_etat_bouton_rouge = etat_bouton_rouge;  
  delay(70);        // delay in between reads for stability
}
