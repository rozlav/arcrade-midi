#define pushButton 5
#define lait2 6
boolean buttonState;

void setup() {
  Serial.begin(9600);
  pinMode(pushButton, INPUT_PULLUP);
  pinMode(lait2, OUTPUT);
}

void loop() {
  int buttonState = digitalRead(pushButton);
  if (buttonState == true) {
    Serial.println("Bouton relâché");
    digitalWrite(lait2,LOW);
  } else {
    Serial.println("Bouton enfoncé");
    digitalWrite(lait2,HIGH);
  }
  
  delay(1);        // delay in between reads for stability
}