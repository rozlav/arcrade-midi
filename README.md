# ArcRade Midi

![fini.jpeg](fini.jpeg)
----

![3-boutons-sans-coque.gif](3-boutons-sans-coque.gif)
![3-boutons-sans-coque.jpeg](3-boutons-sans-coque.jpeg)

## Autodoc

### Misc 
 - https://md.globenet.org/lXcBvaDgQOuH0CoaNjembQ#
 - Most often you connect C to the input pin and NO connected to GND (0V).
 - Pour les +/- du led, le rouge et le noir ne semble pas avoir de signification (ʃ⌣́,⌣́ƪ)

### Menuiserie
 - colle à bois + clous dans les angles
 - pas de fond ?
 - pas de clou sur le plateau du dessus, nécessité de laisser au serre-joint 24h
 - perceuse à colonne grosse mèche/fraise à la taille du pas de visse du bouton + mèche pyramide pour kaler dans l'angle nickel

![schema-menuiserie.jpeg](schema-menuiserie.jpeg)
