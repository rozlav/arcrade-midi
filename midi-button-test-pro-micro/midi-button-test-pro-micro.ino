#include "MIDIUSB.h"

#define pushButton1 2
#define led1 5

bool lastButtonState = false;

void controlChange(byte channel, byte control, byte value) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}

void setup() {
  Serial.begin(9600);
  pinMode(pushButton, INPUT_PULLUP);
  pinMode(lait2, OUTPUT);
}

void loop() {
  int buttonState = digitalRead(pushButton);
  if (buttonState != lastButtonState) {
    if (buttonState == true) {
      digitalWrite(lait2,HIGH);
      Serial.println("On");
      controlChange(0, 10, 0);
      MidiUSB.flush();
    } else {
      digitalWrite(lait2,LOW);
      Serial.println("Off");
      controlChange(0, 10, 127);
      MidiUSB.flush();
    }
  }
  lastButtonState = buttonState;  
  delay(70);        // delay in between reads for stability
}
